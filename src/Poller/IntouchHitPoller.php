<?php

namespace Danjelley\Poller;

use \PDO;
use \Danjelley\HttpClient\Response\IntouchHitPollerResponse;

/**
* IntouchHitPoller - implements details of poller for Intouch Games task spec (database table verification, API call, saving).
*/
class IntouchHitPoller extends Poller
{
	public function process()
	{
		$this->database->createTableIfNotExists();

		$response = $this->client->request();

		$this->database->saveResult($response);
	}
}