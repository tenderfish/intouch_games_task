<?php

namespace Danjelley\Poller;

use Danjelley\Database\Database;
use Danjelley\HttpClient\HttpClient;

/**
* Poller - base class for CRON Pollers for external web services
*/
abstract class Poller
{
	const ENDPOINT = '';
	protected $database;
	protected $client;
	
	public function setDatabase(Database $databaseService)
	{
		$this->database = $databaseService;
	}

	public function setHttpClient(HttpClient $httpClientService)
	{
		$this->client = $httpClientService;
	}
}