<?php

namespace Danjelley\Database;

use \PDO;
use \PDOException;
use \Danjelley\HttpClient\Response\IntouchHitPollerResponse;

/**
* IntouchDatabase - houses some credentials for the IntouchDB, functions to verify / create table and save results.
* Using an object here allows us to swap out a DB or use a different one for other pollers storing elsewhere later.
*/
class IntouchDatabase extends Database
{
	const DATABASE_HOST = DATABASE_HOST;
	const DATABASE_NAME = DATABASE_NAME;
	const DATABASE_USER = DATABASE_USER;
	const DATABASE_PASSWORD = DATABASE_PASSWORD;

	const TABLE_NAME = 'dailystats'; // lower case because I'm using Windows to dev - sorry!

	public function createTableIfNotExists()
	{
		// check if table exists
		// If not, create with getTableSQL
		try{
			$this->query("SELECT 1 FROM `" . static::TABLE_NAME . "` LIMIT 1 ");
		} catch (PDOException $e) {
			// error code 42S02 is 'table doesn't exist', so we should create it
			if($e->getCode() === '42S02') {
				$this->exec($this->getTableSQL());
			} else {
				throw new Exception('An unknown DB error occurred while verifying presence of table');
			}
		}

	}

	public function saveResult(IntouchHitPollerResponse $response)
	{
		$statement = $this->prepare('
			INSERT INTO ' . static::TABLE_NAME . ' (
				`LastUpdate`, 
				`HitCount`, 
				`LastTag`,
				`Date`
			)
			VALUES (
				:lastUpdate, 
				:hitCount, 
				:lastTag,
				NOW()
			) 

			ON DUPLICATE KEY UPDATE 
				`LastUpdate` = VALUES(LastUpdate), 
				`HitCount` = VALUES(HitCount),
				`LastTag` = VALUES(LastTag)
		');

		$lastUpdate = $response->getLastUpdate();
		$hitCount = $response->getHitCount();
		$lastTag = $response->getLastTag();

		$statement->bindParam(':lastUpdate', $lastUpdate, PDO::PARAM_STR);
		$statement->bindParam(':hitCount', $hitCount, PDO::PARAM_INT);
		$statement->bindParam(':lastTag', $lastTag, PDO::PARAM_STR);

		$statement->execute();
	}

	public function getTableSQL()
	{
		// Note : ordinarily I would not use a sql dump for table migrations, this is for speed!
		return "
			-- ----------------------------
			-- Table structure for `dailystats`
			-- ----------------------------
			CREATE TABLE `dailystats` (
			  `Id` int(4) NOT NULL AUTO_INCREMENT,
			  `LastUpdate` datetime DEFAULT NULL,
			  `HitCount` int(11) DEFAULT NULL,
			  `LastTag` varchar(32) DEFAULT NULL,
			  `Date` date DEFAULT NULL,
			  PRIMARY KEY (`Id`),
			  KEY `LastUpdate` (`LastUpdate`) USING BTREE,
			  KEY `HitCount` (`HitCount`) USING BTREE,
			  UNIQUE KEY `Date` (`Date`) USING BTREE
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		";
	}
}