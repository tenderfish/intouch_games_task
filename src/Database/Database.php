<?php

namespace Danjelley\Database;

use \PDO;
use \PDOException;

/**
* Database - base class (singleton). Just a simple wrapper for PDO
*/
abstract class Database
{	
	protected $_db;
	static $_instance;

	// Override these in child classes
	const DATABASE_HOST = '';
	const DATABASE_NAME = '';
	const DATABASE_USER = '';
	const DATABASE_PASSWORD = '';

	private function __clone() {}

	protected function __construct()
	{
		$this->_db = new \PDO('mysql:host=' . static::DATABASE_HOST . ';dbname=' . static::DATABASE_NAME, static::DATABASE_USER, static::DATABASE_PASSWORD);
        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	public static function getInstance()
	{
        if (!(static::$_instance instanceof static)) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }

    // Just forward method calls to PDO object
	public function __call($method, $args) 
	{
	    if(is_callable(array($this->_db, $method))) {
			return call_user_func_array(array($this->_db, $method), $args);
	    } else {
	        throw new BadMethodCallException('Undefined method Database::' . $method);
	    }
	}
}