<?php

namespace Danjelley;

/**
* DependencyInjection - a basic DI container to configure dependencies within this technical task.
* Ordinarily I would use an off-the-shelf DI container for greater power & maintainability, but I 
* wanted to use my own code for this task.
*/
class DependencyInjection
{
	/**
	* This configures which classes get instantiated by the DI container
	*/
	private $config = [
		'IntouchHitPoller' => [
			'class' => '\\Danjelley\\Poller\\IntouchHitPoller',
			'dependencies' => [
				'Database' => '\\Danjelley\\Database\\IntouchDatabase', 
				'HttpClient' => '\\Danjelley\\HttpClient\\IntouchHttpClient'
			]
		]
	];

	/**
	 * This method is responsible for returning a service and injecting dependencies if applicable.
	 * If there is a corresponding configuration in $this->config, it will instantiate classes and assign
	 * them using the set methods on the service class.
	 * @param type $service - name of service to be instantiated
	 * @return type
	 */
	public function get($service)
	{
		if(!isset($this->config[$service])) {
			throw new Exception("Service configuration not found. Please configure this service\'s dependencies in the DI container", 1);
		}

		try {
			
		} catch (Exception $e) {
			echo "Cannot instantiate $service - error : ",  $e->getMessage(), "\n";
			return false;
		}

		$serviceToReturn = new $this->config[$service]['class'];

		if(isset($this->config[$service]['dependencies'])) {

			foreach($this->config[$service]['dependencies'] as $name => $class) {
				try {
					if($this->canConstruct($class)) {
						// regular object, instantiate :
						$serviceToReturn->{'set' . $name}(new $class);
					} else if($this->isSingleton($class)) {
						// singleton, so call 'getInstance';
						$serviceToReturn->{'set' . $name}($class::getInstance());
					} else {
						throw new \Exception("The $class class has no public constructor and no singleton getInstance method.");
					}
				} catch (\Exception $e) {
					echo "Cannot set service dependencies for $service: ",  $e->getMessage(), "\n";
					return false;
				}

			}

		}

		return $serviceToReturn;
	}

	public function isSingleton($class)
	{
		// by convention, I'd have 'getInstance' on a Singleton
		return method_exists($class, 'getInstance');
	}

	public function canConstruct($class)
	{
		// If there's a public __construct method, we can just instantiate it
		if (method_exists($class, '__construct'))
		{
		    $reflection = new \ReflectionMethod($class, '__construct');
		    if (!$reflection->isPublic()) {
		        return false;
		    }
		    
		    return true;
		}

		return false;
	}
}