<?php

namespace Danjelley\HttpClient;

/**
* HttpClient
*/
abstract class HttpClient 
{
	const ENDPOINT = '';

	protected $curl;

	public function __construct()
	{
		$this->curl = curl_init();
		curl_setopt_array($this->curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => static::ENDPOINT,
		));

	}

	public function request()
	{

		$result = curl_exec($this->curl);

		return $this->processResult($result);
	}

	public function processResult($result)
	{
		return $result;
	}
}