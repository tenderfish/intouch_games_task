<?php

namespace Danjelley\HttpClient;

use Danjelley\HttpClient\Response\IntouchHitPollerResponse;

/**
* IntouchHttpClient
*/
class IntouchHttpClient extends HttpClient
{
	const ENDPOINT = INTOUCH_HTTP_CLIENT_ENDPOINT; // From env.php

	public function __construct()
	{
		parent::__construct();

		curl_setopt($this->curl, CURLOPT_POSTFIELDS, ['currentDate' => date('d-m-Y H:i:s', time())]);
	}

	public function processResult($result)
	{
		return new IntouchHitPollerResponse($result);
	}

}