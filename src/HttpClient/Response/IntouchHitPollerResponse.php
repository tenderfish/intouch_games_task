<?php

namespace Danjelley\HttpClient\Response;

/**
* IntouchHitPollerResponse
* ========================
* Structure : Base64encoded binary string with the following data
* bytes 0 - 3 LastUpdate Unix time value stored as signed integer on 32 bits.
* bytes 4 - 7 HitCount Unsigned integer on 32 bits.
* bytes 8 - 31 LastTag Null terminated string maximum 24 characters including tailing 0.
*/
class IntouchHitPollerResponse extends Response
{
	protected $lastUpdate;
	protected $hitCount;
	protected $lastTag;

	function __construct($responseBody)
	{
		if($this->processResponse($responseBody)) {
			parent::__construct($responseBody);

			return;
		}

		throw new \Exception("The response body was invalid.");
	}

	public function processResponse($responseBody)
	{
		$responseBody = base64_decode($responseBody);

		if(strlen($responseBody) != 256) {
			throw new \Exception("Incorrect response length");
		}
		
		$this->lastUpdate = date('Y-m-d H:i:s', bindec(substr($responseBody, 0, 32)));
		$this->hitCount = bindec(substr($responseBody, 32, 32));
		$this->lastTag = $this->convertBinaryToText(substr($responseBody, 64, 192));

		return true;

	}

	// lifted from https://gist.github.com/secrgb/565022
	public function convertBinaryToText($bin)
	{
		$text = '';
		$chars = explode("\n", chunk_split(str_replace("\n", '', $bin), 8));
		$char_count = count($chars);

		# converting the characters one by one
		for($i = 0; $i < $char_count; $text .= chr(bindec($chars[$i])), $i++);

		return $text;
	}

	public function getHitCount()
	{
		return $this->hitCount;
	}

	public function getLastTag()
	{
		return $this->lastTag;
	}

	public function getLastUpdate()
	{
		return $this->lastUpdate;
	}
}