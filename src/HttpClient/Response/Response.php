<?php

namespace Danjelley\HttpClient\Response;

/**
* Response base class
*/
abstract class Response
{
	protected $responseBody;

	function __construct($responseBody)
	{
		$this->responseBody = $responseBody;
	}
}