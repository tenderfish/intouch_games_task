# Dan Jelley Intouch Games Technical Task Submission

### Solution repository :
https://bitbucket.org/tenderfish/intouch_games_task

### Solution API Datasource :
http://intouch.danieljelley.com/datasource.php

### General Notes :
* All classes are my own and coded specifically for this task (nothing off the shelf except for the odd function)
* Environment example file in /env.php.example - copy this to env.php to get up and running with a Database.
* Uses Composer's autoloader - make sure you run `composer install` in the project root if running locally!

#### Class structure :
* Namespaced to `\Danjelley`
* DI Container to instantiate services and manage their dependencies. This could be configured to use mock objects for unit tests, or otherwise provide useful flexibility.
* Poller class implements steps required to carry out polling task - the request, database table setup and saving.
* Response class decodes the base64 encoded binary response. If different data sources implement a different data structure, no problem - implement a new response class!
* Client request class - implements basic curl request - easily expanded to handle HTTP codes, other errors etc.
* Database class - basic wrapper for PDO. IntouchDatabase class implements database access for this specific poller task (configured from DI container).

