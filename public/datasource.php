<?php 

// Note : $_POST['currentDate'] isn't actually used in this script, so while it is posted from the 
// client, it doesn't actually affect the response in any way :)

function generateRandomString($length = 23) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// Have to make sure to pad out to 32 bits when converting integers low enough to be
// represented by fewer bits.
$lastUpdate = str_pad((string)decbin(time()), '32', '0', STR_PAD_LEFT);
$hitCount = str_pad((string)decbin(rand(0,255)), '32', '0', STR_PAD_LEFT);

$string = generateRandomString() . "\0";

$lastTag = '';
for( $i = 0; $i < strlen($string); $i++ ) {
	$char = substr($string, $i, 1);
	$lastTag .= str_pad(decbin(ord($char)), 8, 0, STR_PAD_LEFT);//gmp_strval(gmp_init($packedString[$i+1], 16), 2);
}

echo base64_encode($lastUpdate.$hitCount.$lastTag);
