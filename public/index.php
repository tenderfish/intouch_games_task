<?php 

require('../env.php');
require('../vendor/autoload.php');

use Danjelley\DependencyInjection;
use Danjelley\IntouchHitPoller;

// Class instantiation configured by DI Container 
$dependencyInjection = new DependencyInjection;

// IntouchHitPoller is loaded and configured through a basic DI container.
// The DI Container instantiates the IntouchHitPoller class and injects the relevant dependencies
$intouchHitPoller = $dependencyInjection->get('IntouchHitPoller');

$intouchHitPoller->process();

//$intouchHitPoller->process();

